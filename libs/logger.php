<?php

/**
 * Logger
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 */
class Logger
{
	const INFO = 1;
	const SUCCESS = 2;
	const ERROR = 3;
	const HEADER = 4;

	private $logFile;
	


	public function __construct($logFile)
	{
		$this->logFile = $logFile . date('.Y-m-d');
		
		file_put_contents("$this->logFile.errors", (PHP_EOL . PHP_EOL . PHP_EOL), FILE_APPEND);
		file_put_contents("$this->logFile.all", (PHP_EOL . PHP_EOL . PHP_EOL), FILE_APPEND);
	}



	public function log($message, $type = self::INFO)
	{
		$time = "[" . date('Y-m-d G:i') . "]";

		switch ($type) {
			case self::HEADER:
				$message = "### " . strtoupper($message) . " ###" . PHP_EOL;
				echo PHP_EOL . $message;
				
				file_put_contents("$this->logFile.errors", PHP_EOL . "$time $message", FILE_APPEND);
				file_put_contents("$this->logFile.all", PHP_EOL . "$time $message", FILE_APPEND);
				break;

			case self::SUCCESS:
				$message = "^ $message" . PHP_EOL;
				echo $message;
				
				file_put_contents("$this->logFile.all", "$time $message", FILE_APPEND);
				break;

			case self::INFO:
				$message = "* $message" . PHP_EOL;
				echo $message;

				file_put_contents("$this->logFile.all", "$time $message", FILE_APPEND);
				break;

			case self::ERROR:
				$message = "! $message" . PHP_EOL;
				echo $message;
				
				file_put_contents("$this->logFile.errors", "$time $message", FILE_APPEND);
				file_put_contents("$this->logFile.all", "$time $message", FILE_APPEND);
				break;
		}
	}

}
