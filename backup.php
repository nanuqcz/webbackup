<?php



/********************* Load libs & functions *********************/



require_once('libs/nette.phar');
Nette\Diagnostics\Debugger::enable(TRUE, 'log');

require_once('libs/ftp.class.php');
require_once('libs/logger.php');


function callExtensions($extensions, $params) {
	if (is_array($extensions)) {
		foreach ($extensions as $extension) {
			require_once("extensions/$extension[file]");
			
			call_user_func_array($extension['callback'], $params);
		}
	}
}



/********************* Config *********************/



$config = Nette\Utils\Neon::decode( file_get_contents('config.neon') );



/********************* Backup *********************/



$logger = new Logger('log/backup.log');
foreach ($config['servers'] as $serverName => $serverParams) {
	$logger->log("$serverName", Logger::HEADER);
	

	/*
	 * Create FTP connection
	 */
	// Create FTP
	$ftp = new Ftp();

	// Init log function
	$ftp->logCallbacks[] = function($message, $type) use ($logger){
		$types = array(
			'info' => Logger::INFO,
			'success' => Logger::SUCCESS,
			'error' => Logger::ERROR,
		);
		$logger->log($message, $types[$type]);
	};

	// Connect to server
	$ftp->connect($serverParams['host']);
	if ($serverParams['login']) {
		$ftp->login($serverParams['login'], $serverParams['password']);
	}


	/*
	 * Download files
	 */
	// Prepare local path
	$localPath = "backups/$serverName";

	if (!is_dir($localPath)) {
		$parts = explode('/', $localPath);

		$tempPath = '.';
		foreach ($parts as $part) {
			if (!is_dir("$tempPath/$part")) {
				mkdir("$tempPath/$part");
			}
			$tempPath .= "/$part";
		}
	}

	// Download
	callExtensions(
		$config['extensions']['beforeServerDownload'], 
		array(__DIR__ . "/$localPath", $serverParams)
	);

	$ftp->downloadDirContent($localPath, $serverParams['root'], NULL);
	
	callExtensions(
		$config['extensions']['afterServerDownload'], 
		array(__DIR__ . "/$localPath", $serverParams)
	);


	/*
	 * End
	 */
	$ftp->close();
	
	$logger->log("Download '$serverName' finished.");
}