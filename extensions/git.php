<?php

function extension_prepareGit($projectPath) {
	if (!is_dir("$projectPath/.git")) {
		$cwd = getcwd();

		chdir($projectPath);
		exec("git init");

		chdir($cwd);
	}
}



function extension_commitGit($projectPath) {
	$cwd = getcwd();
	
	chdir($projectPath);

	exec("git add -A");
	exec('git commit -m "' . date('Y-m-d G:i') . '"');

	chdir($cwd);
}



function extension_clearAfterGitCommit($projectPath) {
	$cwd = getcwd();
	
	chdir($projectPath);

	foreach (scandir('.') as $file) {
		if (in_array($file, array('.', '..', '.git')))
			continue;

		if (is_dir($file)) {
			delTree($file);
		} else {
			unlink($file);
		}
	}

	chdir($cwd);
}



if (!function_exists('delTree')) {
	function delTree($dir) {
		$files = array_diff(scandir($dir), array('.','..'));
		foreach ($files as $file) {
			(is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
		}
		return rmdir($dir);
	} 
}